import React, {Component} from 'react';
import './App.css';
import {
    Control,
    Map,
    Placemark,
    YMaps
} from "react-yandex-maps";

class App extends Component {
    state = {
        center: [55.76, 37.64], zoom: 10, route: [
            'Королев',
            {type: 'viaPoint', point: 'Мытищи'},
            'Химки',
            {type: 'wayPoint', point: [55.811511, 37.312518]}
        ]
    };

    render() {
        return (

            <YMaps>
                <Map state={this.state}
                    route={ [
                        {type: 'viaPoint', point: 'Мытищи'},
                        {type: 'wayPoint', point: [55.811511, 37.312518]}
                    ]}
                >


                    {/*<Placemark*/}
                    {/*geometry={{*/}
                    {/*coordinates: [55.751574, 37.573856]*/}
                    {/*}}*/}
                    {/*properties={{*/}
                    {/*hintContent: 'Собственный значок метки',*/}
                    {/*balloonContent: 'Это красивая метка'*/}
                    {/*}}*/}
                    {/*options={{*/}
                    {/*iconLayout: 'default#image',*/}
                    {/*iconImageHref: 'https://demo.phpgang.com/crop-images/demo_files/pool.jpg',*/}
                    {/*iconImageSize: [30, 42],*/}
                    {/*iconImageOffset: [-3, -42]*/}
                    {/*}}*/}
                    {/*/>*/}
                    {/*<Placemark*/}
                    {/*geometry={{*/}
                    {/*coordinates: [55.74457, 37.60118]*/}
                    {/*}}*/}
                    {/*properties={{*/}
                    {/*hintContent: 'DJN',*/}
                    {/*balloonContent: 'Этоgsка'*/}
                    {/*}}*/}
                    {/*options={{*/}
                    {/*iconLayout: 'default#image',*/}
                    {/*iconImageHref: 'https://demo.phpgang.com/crop-images/demo_files/pool.jpg',*/}
                    {/*iconImageSize: [20, 32],*/}
                    {/*iconImageOffset: [-3, -42]*/}
                    {/*}}*/}
                    {/*/>*/}

                </Map>
            </YMaps>
        );
    }
}

export default App;
